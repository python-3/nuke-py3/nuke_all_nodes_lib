__author__ = "Jaime Rivera <jaime.rvq@gmail.com>"
__copyright__ = "Copyright 2022, Jaime Rivera"
__credits__ = []
__license__ = "MIT License"


from all_nodes.logic.logic_node import GeneralLogicNode


class SetNukeFramerange(GeneralLogicNode):

    INPUTS_DICT = {
        "start_frame": {"type": int},
        "end_frame": {"type": int},
    }


class ExecNukeNode(GeneralLogicNode):

    NICE_NAME = "Execute node"
    HELP = ""

    INPUTS_DICT = {
        "node_name_to_execute": {"type": str},
        "first_frame": {"type": int},
        "last_frame": {"type": int},
    }
