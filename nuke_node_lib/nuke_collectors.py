__author__ = "Jaime Rivera <jaime.rvq@gmail.com>"
__copyright__ = "Copyright 2022, Jaime Rivera"
__credits__ = []
__license__ = "MIT License"


from all_nodes.logic.logic_node import GeneralLogicNode


class GetAllNukeNodes(GeneralLogicNode):

    NICE_NAME = "Get all nodes"
    HELP = "Get a dict of all nodes in the scene"

    OUTPUTS_DICT = {"all_nodes_dict": {"type": dict}}


class FilterNukeNodesByClass(GeneralLogicNode):

    INPUTS_DICT = {
        "class_to_filter": {"type": str},
        "delete_filtered_nodes": {"type": bool, "optional": True},
    }

    OUTPUTS_DICT = {"filtered_node_names": {"type": list}}

    def run(self):
        import nuke

        class_to_filter = self.get_attribute_value("class_to_filter")
        delete_filtered_nodes = self.get_attribute_value("delete_filtered_nodes")
        filtered = []
        for n in nuke.allNodes():
            if n.Class() == class_to_filter:
                filtered.append(n.name())
        if delete_filtered_nodes:
            for node_name in filtered:
                nuke.delete(nuke.toNode(node_name))
        self.set_attribute_value("filtered_node_names", filtered)


class GetNukeMainWrite(GeneralLogicNode):

    NICE_NAME = "Get main write"

    OUTPUTS_DICT = {
        "main_write_node_name": {"type": str},
    }


class GetNukeFramerange(GeneralLogicNode):

    OUTPUTS_DICT = {
        "start_frame": {"type": int},
        "end_frame": {"type": int},
    }


